function initMap() {
    var coor = {lat: -0.1081339, lng: -78.4699519};
    var map = new google.maps.Map(document.getElementById('map'), {
    center: coor,
    zoom: 13
    });
    var marker = new google.maps.Marker({
    position: {lat: -0.1081339, lng: -78.4699519},
    map: map
    });
}  