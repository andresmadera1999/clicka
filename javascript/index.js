import {saveTask, onGetTasks} from "./firebase.js";



const taskForm = document.getElementById('formulario')
const taskContainer = document.getElementById('tasks-container')

window.addEventListener('DOMContentLoaded', async () => {
    onGetTasks((querySnapshot) =>{
      let html = ''
      querySnapshot.forEach(doc => {
        const task = doc.data()
        html += `          
              <div class="card-header">
                  <h4 class="my-0 font-weight-bold">${task.Nombre}</h4>
              </div>
              <div class="card-body">
                  <img src="${task.Imagen}" height="500px" width="5px"class="card-img-top">
                  <h1 class="card-title pricing-card-title precio">$ <span class="">${task.Costo}</span></h1>
  
                  <ul class="list-unstyled mt-3 mb-4">
                      <li>${task.Detalle}</li>
                      <li>TALLA S, M, L, XL</li>
                      <li>COLOR AZUL, NEGRO, BLANCO</li>
                  </ul>
                  <a href="" class="btn btn-block btn-primary agregar-carrito" data-id="1">Comprar</a>
              </div>
          `  
      })
      taskContainer.innerHTML = html
  
    })
})


taskForm.addEventListener('submit', (e) =>{
    e.preventDefault()

    const nombre = taskForm['nombre']
    const mail = taskForm['mail']
    const telefono = taskForm['telefono']
    const contrasena = taskForm['contrasena']
    const producto = null
    

    saveTask(nombre.value, mail.value, telefono.value, contrasena.value, producto)
    taskForm.reset()
})

