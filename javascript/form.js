const $form = document.querySelector('#form');
const $buttonMailto = document.querySelector('#email')
$form.addEventListener('submit', handleSubmit)

function handleSubmit(event){
    event.preventDefault();
    const form = new FormData(this);
    $buttonMailto.setAttribute('href',`mailto:jpramirezv@puce.edu.ec?subject=${form.get('asunto')} &body=${form.get('mensaje')} ${form.get('telefono')}`)
    $buttonMailto.click();
    console.log(form.get('nombre'))
}