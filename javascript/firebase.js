// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.2/firebase-app.js";
import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.8.2/firebase-analytics.js";
import { } from "https://www.gstatic.com/firebasejs/9.8.2/firebase-auth.js"
import {getFirestore, collection, addDoc, getDocs, onSnapshot} from "https://www.gstatic.com/firebasejs/9.8.2/firebase-firestore.js"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAGyi7KvPJvukoCO4amF_efDLVhCm-w9VA",
  authDomain: "clickadb-bd987.firebaseapp.com",
  projectId: "clickadb-bd987",
  storageBucket: "clickadb-bd987.appspot.com",
  messagingSenderId: "52453225114",
  appId: "1:52453225114:web:13c4cb24d10724f1cca523",
  measurementId: "G-X45DCMWG6K"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
const id = null;
const db = getFirestore()

export const saveTask = (nombre, mail, telefono, contrasena, producto) =>{
  addDoc(collection(db,'Usuarios'), {nombre, mail, telefono, contrasena, producto});  
}

export const getTasks = () => getDocs(collection(db, 'Productos'))

export const onGetTasks = (callback) => onSnapshot(collection(db, 'Productos'), callback)
